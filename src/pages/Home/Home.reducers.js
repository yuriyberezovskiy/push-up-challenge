import { combineReducers } from 'redux'

import {
  FETCH_USERS,
  FETCH_USERS_SUCCESS,
  FETCH_USERS_ERROR
} from './Home.actions'

import {
  CREATE_USER,
  CREATE_USER_SUCCESS,
  CREATE_USER_ERROR
} from '../NewUser/NewUser.actions'

import {
  ADD_TRAINING_SUCCESS,
  DELETE_USER_SUCCESS,
  UPDATE_USER_SUCCESS,
  DELETE_TRAINING_SUCCESS,
  UPDATE_TRAINING_SUCCESS
} from '../User/User.actions'

const loading = (state = false, action) => {
  switch(action.type) {
    case FETCH_USERS:
    case CREATE_USER:
      return true;
    case FETCH_USERS_SUCCESS:
    case FETCH_USERS_ERROR:
    case CREATE_USER_SUCCESS:
    case CREATE_USER_ERROR:
      return false;
    default:
      return state;
  }
}

const items = (state = [], action) => {
  switch(action.type) {
    case FETCH_USERS_SUCCESS:
      return action.payload;
    case FETCH_USERS_ERROR:
      return [];
    case CREATE_USER_SUCCESS:
      return [
        ...state,
        {
          ...action.payload
        }
      ];
    case UPDATE_USER_SUCCESS:
      return state.map(user => {
        if (user.id === action.payload.id) {
          return {
            ...user,
            ...action.payload
          }
        }
        return user
      })
    case DELETE_USER_SUCCESS:
      return state.filter(user => user.id !== action.payload.id)
    case ADD_TRAINING_SUCCESS:
    case DELETE_TRAINING_SUCCESS:
    case UPDATE_TRAINING_SUCCESS:
      return state.map(item => {
        if (item.id === +action.payload.day.user.id) {
          return user(item, action)
        }
        return item;
      })
    default:
      return state;
  }
}

const user = (state = {}, action) => {
  switch(action.type) {
    case ADD_TRAINING_SUCCESS:
    case DELETE_TRAINING_SUCCESS:
    case UPDATE_TRAINING_SUCCESS:
      return {
        ...state,
        days: state.days.map(item => {
          if (item.day_number === action.payload.day.day_number) {
            return day(item, action)
          }
          return item
        })
      }
    default:
      return state;
  }
}

const day = (state = {}, action) => {
  switch(action.type) {
    case ADD_TRAINING_SUCCESS:
      return {
        ...state,
        push_ups: state.push_ups + action.payload.amount,
        times: state.times + 1
      }
    case DELETE_TRAINING_SUCCESS:
      return {
        ...state,
        push_ups: state.push_ups - action.payload.amount,
        times: state.times - 1
      }
    case UPDATE_TRAINING_SUCCESS:
      return {
        ...state,
        push_ups: action.payload.day.push_ups
      }
    default:
      return state;
  }
}

export default combineReducers({
  loading,
  items
})