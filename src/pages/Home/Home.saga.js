import { call, put, takeEvery } from 'redux-saga/effects';
import Api from '../../common/Api/Api';
import {
  FETCH_USERS,
  fetchUsersSuccess,
  fetchUsersError,
} from './Home.actions'

function loadUsers() {
  return Api.get('users')
    .then(({data}) => data)
}

function* getUsers() {
  try {
    const users = yield call(loadUsers)
    yield put(fetchUsersSuccess(users))
  } catch (error) {
    yield put(fetchUsersError(error))
  }
}

export default [
  takeEvery(FETCH_USERS, getUsers)
]