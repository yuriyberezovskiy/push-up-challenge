import { createSelector } from 'reselect'
import moment from 'moment'
import { START_DATE } from '../../config/dates'

const calcPushUps = (users) => {
  return users.map(user => ({
    ...user,
    push_ups: user.days.reduce((acc, item) => (acc + item.push_ups), 0)
  }));
}

export const getSortedByPushUps = createSelector(
  [calcPushUps],
  (users) => users.sort((a, b) => (b.push_ups - a.push_ups))
)

export const getUsersWithHiddenDays = createSelector(
  [getSortedByPushUps],
  (users) => {
    return users.map(user => ({
      ...user,
      days: user.days.map((day, index) => ({
        ...day,
        hidden: moment().subtract(index+1, 'day') < moment(START_DATE, 'L')
      }))
    }))
  }
)