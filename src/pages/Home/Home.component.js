import React from 'react'
import { Grid, Table, Button, Loader, Icon, Popup, Input } from 'semantic-ui-react'
import { Link } from 'react-router-dom'
import AccessRights from '../../common/AccessRights/AccessRights.container'
import moment from 'moment'
import { START_DATE } from '../../config/dates';

const Home = ({ tableSearchInput, items, days, loading, clickSelect, clickDelete, toggleExpandTable, shouldShowAllDays, addHover, onTableMouseLeave, hoveredRowIndex, shouldShowSearch, toggleSearch, onUpdateTableFilter}) => (
  <React.Fragment>
    {!loading ? (
    <Grid columns={2}>
      <Grid.Row>
        <Grid.Column width={2} className="users">
          <Table selectable unstackable>
            <Table.Header>
              <Table.Row>
                <Table.HeaderCell rowSpan='2'>
                  <div>
                    Participants
                    {shouldShowSearch ? (
                      <Icon onClick={toggleSearch} link bordered name='delete' aria-label='hide search' />
                    ) : (
                      <Icon onClick={toggleSearch} link bordered name='search' aria-label='show search' />
                    )}
                  </div>
                  {shouldShowSearch && (
                    <Input ref={tableSearchInput} onKeyUp={(e) => {onUpdateTableFilter(e.target.value)}} style={{maxWidth: '150px', marginTop: '5px'}} size='mini' placeholder='Search users...' />
                  )}
                </Table.HeaderCell>
                <Table.HeaderCell>###</Table.HeaderCell>
              </Table.Row>
              <Table.Row>
                <Table.HeaderCell>###</Table.HeaderCell>
              </Table.Row>
            </Table.Header>

            <Table.Body>
              {items.length ? items.map((user, index) => (
                <Table.Row key={user.id} className={(index === hoveredRowIndex) ? 'hovered' : null} onMouseOver={ (() => addHover(index)) } onMouseLeave={onTableMouseLeave}>
                  <Table.Cell onClick={(() => clickSelect(user.id))} >
                    {index === 0 && <Icon title='1 place' name='winner' style={{color: '#FFD700'}} />}
                    {index === 1 && <Icon title='2 place' name='winner' style={{color: '#c5c9c7'}} />}
                    {index === 2 && <Icon title='3 place' name='winner' style={{color: '#cd7f32'}} />}
                    {user.username}
                    {user.finish_date && <Icon title='finished' name='man' style={{fontSize: '1.2em', lineHeight: 1}} />}
                  </Table.Cell>
                </Table.Row>
              )) : null}
            </Table.Body>
          </Table>
        </Grid.Column>

        <Grid.Column width={14} className="pushups clickable">
          <Table unstackable selectable>
            <Table.Header>
              <Table.Row>
                <Table.HeaderCell textAlign='center' colSpan={days.filter(day => shouldShowAllDays ? true : !day.hidden).length}>Days</Table.HeaderCell>
                <Table.HeaderCell />
                {!shouldShowAllDays && <Table.HeaderCell />}
                <AccessRights>
                  <Table.HeaderCell />
                </AccessRights>
              </Table.Row>
              <Table.Row>
                {days.filter(day => shouldShowAllDays ? true : !day.hidden).map((item, index) => (
                  <Popup key={item.day}
                    trigger={
                      <Table.HeaderCell className={ item.isCurrent && shouldShowAllDays ? 'clickable' : item.isCurrent ? 'is-current' : null} onClick={(item.isCurrent && shouldShowAllDays) ? toggleExpandTable : null}>
                        <div style={{display: 'flex', whiteSpace: 'nowrap'}}>
                          {item.day}
                          {item.isCurrent && shouldShowAllDays && (
                            <span>&nbsp;...<Icon name='arrow left' /></span>
                          )}
                        </div>
                      </Table.HeaderCell>
                    } content={moment(START_DATE, 'L').add(index, 'days').format('MMM Do YYYY')} />
                ))}
                {!shouldShowAllDays && days.filter(day => day.hidden).length ? (
                  <Table.HeaderCell onClick={toggleExpandTable} title='Show all days!' className='clickable'>
                    <div>> 30</div>
                  </Table.HeaderCell>
                ) : (
                  null
                )}
                <Table.HeaderCell>
                  &#931;
                </Table.HeaderCell>
                <AccessRights>
                  <Table.HeaderCell />
                </AccessRights>
              </Table.Row>
            </Table.Header>

            <Table.Body>
              {items.length ? items.map((user, index) => (
                <Table.Row key={user.id} onClick={(() => clickSelect(user.id))} className={(index === hoveredRowIndex) ? 'hovered' : null} onMouseOver={ (() => addHover(index)) } onMouseLeave={onTableMouseLeave}>
                  {user && user.days.filter(day => shouldShowAllDays ? true : !day.hidden).map(day => (
                    <Table.Cell className={ day.is_current ? 'is-current' : day.push_ups >= 100 ? 'highlight' : null} key={day.day_number}>
                      {day.push_ups}
                    </Table.Cell>
                  ))}
                  {!shouldShowAllDays && user && user.days.filter(day => day.hidden).length ? (
                    <Table.Cell textAlign='center' className='clickable'>
                      ...
                    </Table.Cell>
                  ) : (
                    null
                  )}
                  <Table.Cell className='highlight highlight--red'>
                    {user.push_ups}
                  </Table.Cell>
                  <AccessRights>
                    <Table.Cell textAlign='center'>
                      <Icon link name='delete' onClick={(e) => clickDelete(e, user.id)} />
                    </Table.Cell>
                  </AccessRights>
                </Table.Row>
              )) : null}
            </Table.Body>
          </Table>
        </Grid.Column>
      </Grid.Row>
    </Grid>
    ) : (
      <Loader active inline='centered'>Loading...</Loader>
    )}
    <AccessRights>
      <Link style={{position: 'fixed', right: '20px', bottom: '80px'}} to='/add-user' title='add participant'>
        <Button color='black' title='Back to home page' circular icon='plus' />
      </Link>
    </AccessRights>
  </React.Fragment>
);

export default Home;