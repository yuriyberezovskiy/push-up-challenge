import React, { Component } from 'react'
import { connect } from 'react-redux'
import Home from './Home.component'
import { push } from 'connected-react-router'
import { confirmAlert } from 'react-confirm-alert'
import Confirm from '../../common/Dialogs/Confirm/Confirm.component'
import 'react-confirm-alert/src/react-confirm-alert.css'
import { fetchUsers } from './Home.actions'
import { deleteUser } from '../User/User.actions'
import { getUsersWithHiddenDays } from './Home.selectors'
import { updateTableFilter } from '../../common/components/Table/Table.actions'

class HomeContainer extends Component {
  constructor(props) {
    super(props)

    this.state = {
      shouldShowAllDays: false,
      hoveredRowIndex: -1,
      shouldShowSearch: false
    }

    this.clickDelete = this.clickDelete.bind(this)
    this.toggleExpandTable = this.toggleExpandTable.bind(this)
    this.addHover = this.addHover.bind(this)
    this.onTableMouseLeave = this.onTableMouseLeave.bind(this)
    this.toggleSearch = this.toggleSearch.bind(this)

    this.tableSearchInput = React.createRef()
  }

  toggleSearch() {
    this.setState((prevState => ({
      shouldShowSearch: !prevState.shouldShowSearch
    })),
    () => {
      if (this.state.shouldShowSearch) {
        this.tableSearchInput.current.focus();
      }
    })
  }

  toggleExpandTable() {
    this.setState((prevState => ({
      shouldShowAllDays: !prevState.shouldShowAllDays
    })))
  }

  addHover(index) {
    this.setState({
      hoveredRowIndex: index
    })
  }

  onTableMouseLeave() {
    this.setState({
      hoveredRowIndex: -1
    })
  }

  componentDidMount() {
    let {
      getUsers,
      items
    } = this.props

    if (!items.length) {
      getUsers()
    }
  }

  clickDelete(e, id) {
    e.stopPropagation()

    confirmAlert({
      customUI: ({onClose}) => (
        <Confirm
          onClose={onClose}
          onConfirm={() => {this.props.onDeleteUser(id)}}
          content='Are you sure you want to delete the participant?'
        />
      )
    })
  }

  render() {
    let {
      shouldShowAllDays,
      hoveredRowIndex,
      shouldShowSearch
    } = this.state;

    return (
      <Home
        { ...this.props }
        tableSearchInput={this.tableSearchInput}
        shouldShowSearch={shouldShowSearch}
        toggleSearch={this.toggleSearch}
        clickDelete={ this.clickDelete }
        shouldShowAllDays={ shouldShowAllDays }
        toggleExpandTable={ this.toggleExpandTable }
        addHover={this.addHover}
        onTableMouseLeave={this.onTableMouseLeave}
        hoveredRowIndex={hoveredRowIndex}
      />
    )
  }
}

const mapStateToProps = ({days, users: {loading, items}, table: { filter }}) => ({
  days,
  loading,
  items: getUsersWithHiddenDays(items).filter(item => item.username.toLowerCase().indexOf(filter.toLowerCase()) !== -1)
});

const mapDispatchesToProps = (dispatch) => ({
  getUsers() {
    dispatch(fetchUsers())
  },
  clickSelect(id) {
    dispatch(push(`/user/${id}${window.location.search}`))
  },
  onDeleteUser(id) {
    dispatch(deleteUser(id))
  },
  onUpdateTableFilter(str) {
    dispatch(updateTableFilter(str))
  }
})

export default connect(
  mapStateToProps,
  mapDispatchesToProps
)(HomeContainer);