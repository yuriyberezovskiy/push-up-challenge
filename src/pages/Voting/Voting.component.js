import React from 'react'
import { Grid, Header, Form, Message, Icon } from 'semantic-ui-react'
import Target from './Target/Target.container'
import Items from './Items/Items.container'
import { Link } from 'react-router-dom'

const Voting = ({ items, selectedItems, onSubmit, onChanged, username, email, loading, onMoved, onBack, onHover, error, submitted, success, loadingItems }) => (
  <React.Fragment>
    {error && (
      <Message negative style={{marginBottom: '20px'}}>
        <Message.Header>We're sorry we can't process that request</Message.Header>
        <p>{ error.errorMessage }</p>
      </Message>
    )}
    {success ? (
      <div>
        <Message
          success
          header='Congratulations, Your prediction was accepted!'
          content='If your prediction will be correct, you will get nice gifts!'
        />
        <Link to="/">
          <Icon name='arrow left' /> Back
        </Link>
      </div>
    ) : (
      <Grid>
        <Grid.Row>
          <Grid.Column width='4'>
          <Header as='h3'>
            Fun prediction!
          </Header>

          <p style={{fontSize: '1.1em'}}>
            Try your luck and make marathon prediction!
          </p>

          <p style={{fontSize: '1.1em'}}>
            Select participants,
            that on you think will be able to finish <b>Push Ups marathon</b> (3000 push ups till final day) and place them in correct sequence!
          </p>

          <p style={{fontSize: '1.1em'}}>
            For those lucky who will guess everything we already have a big surprise!
          </p>

          <Form onSubmit={onSubmit}>
            <Form.Input required type="text" placeholder="Name" name='voting_name' value={username} onChange={onChanged} />
            <Form.Input required type="email" placeholder='Email' name='voting_email' value={email} onChange={onChanged} />

            <Form.Button loading={loading} type="submit" secondary style={{ width: '100%' }}>
                Make prediction!
              </Form.Button>
          </Form>
          </Grid.Column>
          <Grid.Column width='4'>
            <Items items={items} loading={loadingItems} />
          </Grid.Column>
          <Grid.Column width='8'>
            <Target onMoved={onMoved} onBack={onBack} onHover={onHover} items={selectedItems} submitted={ submitted } />
          </Grid.Column>
        </Grid.Row>
      </Grid>
    )}
  </React.Fragment>
)

export default Voting