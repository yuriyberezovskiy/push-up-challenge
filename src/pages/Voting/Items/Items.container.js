import React from 'react'
import Item from './Items.component'
import { Loader } from 'semantic-ui-react'

class ItemsContainer extends React.Component {
  render() {
    let {
      items,
      onMove,
      loading
    } = this.props

    return (
      loading ? (
        <Loader active inline='centered'>Loading...</Loader>
      ) : (
        items.filter(item => !item.moved).map((item, index) => (
          <Item key={item.id} index={index} item={item} onMove={onMove} />
        ))
      )
    )
  }
}

export default ItemsContainer