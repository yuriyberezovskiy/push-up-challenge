import React from 'react'
import { DragSource } from 'react-dnd'
import { Label } from 'semantic-ui-react'

const itemSource = {
  beginDrag(props) {
    return {
      ...props.item
    }
  },
  endDrag(props, monitor, component) {
    console.log('end item drag')
  }
}

function collect(connect, monitor, props) {
  return {
    connectDragSource: connect.dragSource(),
    isDragging: monitor.isDragging()
  }
}

const type = 'voting'

class Item extends React.Component {
  render() {
    let {
      item,
      connectDragSource,
      isDragging,
      isOver
    } = this.props

    return (
      connectDragSource(
        <div key={ item } style={{
          opacity: isDragging ? 0.5 : 1,
          border: isOver ? '1px solid gray' : '1px dashed gray',
          backgroundColor: 'white',
          padding: '0.5rem 1rem',
          marginRight: '1rem',
          marginBottom: '1rem',
          cursor: 'move'
        }}>
          <span>{ item.username }</span>
          <Label color='red' horizontal style={{float: 'right'}}>
          { item.push_ups }
          </Label>
        </div>
    ))}
}

export default DragSource(type, itemSource, collect)(Item)