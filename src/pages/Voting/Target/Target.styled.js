import styled, { css } from 'styled-components'

const isOver = css`
  border-width: 4px;
`

const error = css`
  border-color: red;
  .hint {
    color: red !important;
  }
`

const TargetStyled = styled.div`
  display: flex;
  flex-direction: column;
  flex: 1;
  overflow-y: auto;
  height: 100%;
  min-height: 400px;
  border: 2px dashed #ccc;
  ${props => props.isOver ? isOver : null}
  ${props => props.error ? error : null}

  .hint {
    margin: auto;
    font-size: 2em;
    color: #ccc;
    font-weight: bold;
  }

  .list {
    width: 100%;
  }

  .icon {
    float: right;
    cursor: pointer;
  }
`

export default TargetStyled