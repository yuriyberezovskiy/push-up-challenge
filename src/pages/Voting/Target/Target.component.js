import React from 'react'
import { Icon, Label } from 'semantic-ui-react'
import { DropTarget, DragSource } from 'react-dnd'
import { findDOMNode } from 'react-dom'

const type = 'voting'

const dropSource = {
  drop(props, monitor, component) {
    if(!component) return undefined

    let item = monitor.getItem()
    let index = props.index
    
    props.handleMoved(item, index)
    return item
  },
  hover(props, monitor, component) {
    if (!component) {
      return null
    }
    const item = monitor.getItem()
    const dragIndex = monitor.getItem().index
    const hoverIndex = props.index

    // Don't replace items with themselves
    if (dragIndex === hoverIndex) {
      return
    }

    // Determine rectangle on screen
    const hoverBoundingRect = findDOMNode(component).getBoundingClientRect()

    // Get vertical middle
    const hoverMiddleY = (hoverBoundingRect.bottom - hoverBoundingRect.top) / 2

    // Determine mouse position
    const clientOffset = monitor.getClientOffset()

    // Get pixels to the top
    const hoverClientY = clientOffset.y - hoverBoundingRect.top

    // Only perform the move when the mouse has crossed half of the items height
    // When dragging downwards, only move when the cursor is below 50%
    // When dragging upwards, only move when the cursor is above 50%
    // Dragging downwards
    if (dragIndex < hoverIndex && hoverClientY < hoverMiddleY) {
      return
    }

    // Dragging upwards
    if (dragIndex > hoverIndex && hoverClientY > hoverMiddleY) {
      return
    }

    // Time to actually perform the action
    props.onHover(item, hoverIndex)

    // Note: we're mutating the monitor item here!
    // Generally it's better to avoid mutations,
    // but it's good here for the sake of performance
    // to avoid expensive index searches.
    monitor.getItem().index = hoverIndex
  }
}

function dropCollect(connect, monitor) {
  return {
    connectDropTarget: connect.dropTarget(),
    canDrop: monitor.canDrop(),
    isOver: monitor.isOver()
  }
}

const dragSource = {
  beginDrag(props) {
    return {
      ...props.item
    }
  }
}

function dragCollect(connect, monitor, props) {
  return {
    connectDragSource: connect.dragSource(),
    isDragging: monitor.isDragging()
  }
}

class Target extends React.Component {
  render() {
    let {
      item,
      connectDropTarget,
      onBack,
      index,
      connectDragSource,
      isDragging
    } = this.props

    return(
      connectDragSource(
        connectDropTarget(
          <div key={item.id} style={{
            border: '2px dashed #ccc',
            backgroundColor: 'white',
            padding: '0.5rem 1rem',
            margin: '1rem 1rem 0 1rem',
            cursor: 'move',
            fontSize: '1.2em',
            opacity: (isDragging || item.dragging) ? 0 : 1
          }}>
            <b>
              { ++index }.&nbsp;&nbsp;
            </b>
            <span>
              {item.username}
            </span>
            <Icon className='icon' onClick={() => onBack(item)} name='delete' />
            <Label color='red' horizontal style={{float: 'right'}}>
              { item.push_ups }
            </Label>
          </div>
        )
      )
    )
  }
}

export default DragSource(type, dragSource, dragCollect)(DropTarget(type, dropSource, dropCollect)(Target))