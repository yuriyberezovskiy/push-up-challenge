import React from 'react'
import { DropTarget } from 'react-dnd'
import Target from './Target.component'
import TargetStyled from './Target.styled'

const type = 'voting'

const dropSource = {
  drop(props, monitor, component) {
    let didDrop = monitor.didDrop()
    if (didDrop) return undefined

    if (!component) return undefined

    let item = monitor.getItem()
    component.handleMoved(item, 50)
    return item
  }
}

function dropCollect(connect, monitor) {
  return {
    connectDropTarget: connect.dropTarget(),
    canDrop: monitor.canDrop(),
    isOver: monitor.isOver({ shallow: true })
  }
}

class TargetContainer extends React.Component {
  constructor() {
    super()

    this.handleMoved = this.handleMoved.bind(this)
  }

  handleMoved(item, index) {
    let { onMoved } = this.props

    if (item) {
      onMoved(item, index)
    }
  }

  render() {
    let { connectDropTarget, items, isOver, submitted} = this.props

    return (
      connectDropTarget(
        <div>
        <TargetStyled isOver={isOver} error={submitted && !items.length}>
          {!items.length ? (
            <div className='hint'>Drop users here!</div>
          ) : null}
          {items.map((item, index) => (
            <Target {...this.props} item={item} key={item.id} index={index} handleMoved={this.handleMoved} />
          ))}
        </TargetStyled>
        </div>
      )
    )
  }
}

export default DropTarget(type, dropSource, dropCollect)(TargetContainer)