import React from 'react'
import { connect } from 'react-redux'
import Voting from './Voting.component'
import { DragDropContext } from 'react-dnd'
import HTML5Backend from 'react-dnd-html5-backend'
import { fetchUsers } from '../Home/Home.actions'
import isEqual from 'lodash/isEqual'
import Api from '../../common/Api/Api'
import { getSortedByPushUps } from '../Home/Home.selectors'

class VotingContainer extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      items: [],
      selectedItems: [],
      error: false,
      voting_name: '',
      voting_email: '',
      loading: false,
      submitted: false,
      success: false
    }

    this.onMoved = this.onMoved.bind(this)
    this.onBack = this.onBack.bind(this)
    this.onHover = this.onHover.bind(this)

    this.onChanged = this.onChanged.bind(this)
    this.onSubmit = this.onSubmit.bind(this)
  }

  componentDidMount() {
    let { items, getUsers } = this.props

    if (!items.length) {
      getUsers()
    } else {
      this.setState({
        items: items.map((item, index) => ({
          ...item, 
          index
        }))
      })
    }
  }

  componentDidUpdate(prevProps) {
    if (!isEqual(prevProps.items, this.props.items)) {
      this.setState({
        items: this.props.items.map((item, index) => ({
          ...item, 
          index
        }))
      })
    }
  }

  onMoved(moved, index) {
    let { selectedItems } = this.state

    selectedItems = selectedItems.filter(item => item.id !== moved.id)
    selectedItems = [
      ...selectedItems.slice(0, index),
      {
        ...moved
      },
      ...selectedItems.slice(index)
    ]

    this.setState(prevState => ({
      selectedItems: selectedItems.map((item, index) => ({
        ...item,
        index
      })),
      items: prevState.items.filter(item => item.id !== moved.id)
    }))
  }

  onHover(moved, index) {
    let { selectedItems } = this.state

    selectedItems = selectedItems.filter(item => item.id !== moved.id)

    this.setState(prevState => ({
      selectedItems: [
        ...selectedItems.slice(0, index),
        {
          ...moved,
          index,
          dragging: true
        },
        ...selectedItems.slice(index)
      ]
    }))
  }
  
  onBack(moved) {
    this.setState(prevState => ({
      items: [
        moved,
        ...prevState.items
      ],
      selectedItems: prevState.selectedItems.filter(item => item.id !== moved.id)
    }))
  }

  onChanged(e, {name, value}) {
    this.setState({
      [name]: value
    });
  }

  onSubmit() {
    let {
      selectedItems,
      voting_name,
      voting_email
    } = this.state

    this.setState({
      submitted: true
    })

    if (!voting_name || !voting_email || !selectedItems.length) {
      this.setState({
        error: {
          errorMessage: `Unable to process shipping information. Please check input data.`
        }
      })

      return
    }

    this.setState({
      error: false,
      loading: true
    })

    return Api.post('votings', {
      voting_name,
      voting_email,
      votes: selectedItems.map((item, index) => ({
        user: item.id,
        place: index+1
      }))
    })
      .then(({data}) => {
        console.log('prediction was set successfully')

        this.setState({
          error: false,
          loading: false,
          success: true
        })
      })
      .catch(({response}) => {
        this.setState({
          error: {
            errorMessage: response.data.errorMessage
          },
          loading: false
        })
      })
  }

  render() {
    let {
      items,
      selectedItems,
      loading,
      error,
      submitted,
      success
    } = this.state

    let {
      loadingItems
    } = this.props

    return (
      <Voting loadingItems={loadingItems} items={ items } selectedItems={ selectedItems } loading={ loading } error={ error } submitted={ submitted } success={ success }
        onChanged={this.onChanged}
        onSubmit={this.onSubmit}
        onMoved={this.onMoved}
        onHover={this.onHover}
        onBack={this.onBack} />
    )
  }
}

const mapStateToProps = ({users: {items, loading}}) => ({
  items: getSortedByPushUps(items),
  loadingItems: loading
})

const mapDispatchToProps = (dispatch) => ({
  getUsers() {
    dispatch(fetchUsers())
  }
})

export default DragDropContext(HTML5Backend)(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(VotingContainer)
)