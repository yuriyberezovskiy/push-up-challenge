import {
  FETCH_USER,
  FETCH_USER_SUCCESS,
  FETCH_USER_ERROR,
  ADD_TRAINING,
  ADD_TRAINING_SUCCESS,
  ADD_TRAINING_ERROR,
  UPDATE_TRAINING_SUCCESS,
  UPDATE_USER_SUCCESS,
  DELETE_TRAINING,
  DELETE_TRAINING_ERROR,
  DELETE_TRAINING_SUCCESS,
  SELECT_DAY
} from './User.actions';

import { combineReducers } from 'redux';

const loading = (state = false, action) => {
  switch(action.type) {
    case FETCH_USER:
      return true;
    case FETCH_USER_SUCCESS:
    case FETCH_USER_ERROR:
      return false;
    default:
      return state;
  }
}

const selected = (state = {}, action) => {
  switch(action.type) {
    case FETCH_USER_SUCCESS:
      return action.payload;
    case UPDATE_USER_SUCCESS:
      return {
        ...state,
        ...action.payload
      }
    case ADD_TRAINING_SUCCESS:
    case UPDATE_TRAINING_SUCCESS:
    case DELETE_TRAINING_SUCCESS:
      return {
        ...state,
        days: state.days.map(item => {
          if (item.day_number === action.payload.day.day_number) {
            return day(item, action)
          }
          return item;
        })
      }
    case FETCH_USER_ERROR:
      return {};
    case ADD_TRAINING_ERROR:
    default:
      return state;
  }
}

const loadingForDay = (state = false, action) => {
  switch(action.type) {
    case ADD_TRAINING:
    case DELETE_TRAINING:
      return true;
    case ADD_TRAINING_SUCCESS:
    case ADD_TRAINING_ERROR:
    case DELETE_TRAINING_SUCCESS:
    case DELETE_TRAINING_ERROR:
      return false;
    default:
      return state;
  }
}

const selectedOFDay = (state ={}, action) => {
  switch(action.type) {
    case SELECT_DAY:
      return action.payload
    case ADD_TRAINING_SUCCESS:
      return day(state, action)
    case UPDATE_TRAINING_SUCCESS:
      return day(state, action)
    case DELETE_TRAINING_SUCCESS:
      return day(state, action)
    case ADD_TRAINING_ERROR:
    default:
      return state;
  }
}

const day = (state = {}, action) => {
  switch(action.type) {
    case SELECT_DAY:
      return action.payload
    case ADD_TRAINING_SUCCESS:
      return {
        ...state,
        push_ups: state.push_ups + action.payload.amount,
        trainings: trainings(state.trainings, action)
      }
    case DELETE_TRAINING_SUCCESS:
      return {
        ...state,
        push_ups: state.push_ups - action.payload.amount,
        trainings: trainings(state.trainings, action)
      }
    case UPDATE_TRAINING_SUCCESS:
      return {
        ...state,
        push_ups: state.push_ups - state.trainings.find(({id}) => id === action.payload.id)['amount'] + action.payload.amount,
        trainings: trainings(state.trainings, action)
      }
    default:
      return state;
  }
}

const trainings = (state = [], action) => {
  switch(action.type) {
    case ADD_TRAINING_SUCCESS:
      let {
        day,
        ...training
      } = action.payload

      return [
        ...state,
        training
      ];
    case DELETE_TRAINING_SUCCESS:
      return state.filter(item => item.id !== action.payload.id)
    case UPDATE_TRAINING_SUCCESS:
      return state.map(item => {
        if (item.id === action.payload.id) {
          return {
            ...item,
            amount: action.payload.amount
          }
        }
        return item;
      })
    default:
      return state;
  }
}

const selectedDay = combineReducers({
  selected: selectedOFDay,
  loading: loadingForDay
})

export default combineReducers({
  selected,
  loading,
  selectedDay
})