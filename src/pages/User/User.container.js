import React from 'react'
import { connect } from 'react-redux'
import User from './User.component'
import withAccessRights from '../../common/AccessRights/withAccessRights'
import { fetchUser, addTraining, updateUser, selectDay } from './User.actions'
import { getMaxPushUpsFromDay, getMaxPushUpsFromTrainings, getAmountUserPushUps } from './User.selectors'
import moment from 'moment'
import { START_DATE } from '../../config/dates'
import { powerStatuses } from '../../config/user'

class UserContainer extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      isModalOpen: false,
      pushUps: '',
      error: false,
      maxPushUpsForPeriod: null,
      powerStatus: ''
    };

    this.onCloseModal = this.onCloseModal.bind(this);
    this.onSelectDay = this.onSelectDay.bind(this);
    this.onAddTraining = this.onAddTraining.bind(this);
    this.onChanged = this.onChanged.bind(this);
    this.calcUserPower = this.calcUserPower.bind(this);
  }

  componentDidMount() {
    let {
      match,
      getUser,
      selected
    } = this.props;

    if (selected.id !== +match.params.id) {
      getUser(match.params.id);
    }
  }

  onSelectDay(day) {
    if (!this.props.checkAccessRights()) return;

    this.setState({
      isModalOpen: true
    })

    this.props.handleSelectDay(day)
  }

  onCloseModal() {
    this.setState({
      isModalOpen: false,
      pushUps: ''
    })
  }

  onAddTraining(e) {
    e.preventDefault();
    let { pushUps } = this.state
    let { selected, addTraining, selectedDay } = this.props

    if (parseInt(pushUps, 10) > 0) {
      addTraining({
        user: selected.id,
        amount: pushUps,
        day: selectedDay.day_number
      })

      this.setState({
        pushUps: '',
      })
    }
  }

  onChanged(e, { value, name }) {
    this.setState({
      [name]: value
    })
  }

  calcUserPower() {
    let {
      amountPushUps
    } = this.props

    let maxPushUpsForPeriod = moment().diff(moment(START_DATE, 'L'), 'days') * 100
    let powerIndex = Math.floor(amountPushUps/maxPushUpsForPeriod*100)
    let powerStatus

    if (powerIndex >= 100) {
      powerStatus = powerStatuses[0]
    } else if (powerIndex >= 75) {
      powerStatus = powerStatuses[1]
    } else if (powerIndex >= 50) {
      powerStatus = powerStatuses[2]
    } else if (powerIndex >= 25) {
      powerStatus = powerStatuses[3]
    } else {
      powerStatus = powerStatuses[4]
    }

    if (powerIndex > powerStatuses.length-1) {
      powerIndex = powerStatuses.length-1
    }

    this.setState({
      powerStatus,
      maxPushUpsForPeriod
    })
  }

  render() {
    let {
      isModalOpen,
      maxPushUpsForPeriod,
      powerStatus,
      pushUps
    } = this.state;

    return (
      <User {...this.props} isModalOpen={isModalOpen}
        onChanged={this.onChanged}
        onAddTraining={this.onAddTraining}
        onSelectDay={this.onSelectDay}
        calcUserPower={this.calcUserPower}
        maxPushUpsForPeriod={maxPushUpsForPeriod}
        powerStatus={powerStatus}
        onCloseModal={this.onCloseModal}
        pushUps={pushUps} />
      )
  }
}

const mapStateToProps = (state) => ({
  selected: state.user.selected,
  loading: state.user.loading,
  selectedDay: state.user.selectedDay && state.user.selectedDay.selected,
  loadingForDay: state.user.selectedDay && state.user.selectedDay.loading,
  maxPushUpsPerDay: getMaxPushUpsFromDay(state.user.selected),
  maxPushUpsPerTraining: getMaxPushUpsFromTrainings(state.user.selected),
  amountPushUps: getAmountUserPushUps(state)
})

const mapDispatchesToProps = (dispatch, { match }) => ({
  getUser(id) {
    dispatch(fetchUser(id))
  },
  addTraining(obj) {
    dispatch(addTraining(obj))
  },
  patchUser(patch) {
    dispatch(updateUser({
      id: match.params.id,
      ...patch
    }))
  },
  handleSelectDay(day) {
    dispatch(selectDay(day));
  }
})

export default withAccessRights(
  connect(
    mapStateToProps,
    mapDispatchesToProps
  ) (UserContainer)
);