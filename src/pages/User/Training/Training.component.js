import React from 'react'
import StyledTraining from './Training.styled'
import { Icon } from 'semantic-ui-react'

const Training = ({ item, index, onUpdate, onSubmit, onDeleteTraining, onChange, inputRef, calculatedWidth }) => (
  <StyledTraining onSubmit={onSubmit}>
    <label>{++index}. Training</label>
    <input ref={inputRef} style={{width: calculatedWidth}} type='number' defaultValue={item.amount} name='training' onChange={onChange} onBlur={onUpdate} />
    <Icon className='delete-icon' name='delete' onClick={onDeleteTraining} />
  </StyledTraining>
)

export default Training