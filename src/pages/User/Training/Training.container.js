import React from 'react'
import { connect } from 'react-redux'
import Training from './Training.component'
import { updateTraining, deleteTraining } from '../../User/User.actions'
import { withRouter } from 'react-router'

class TrainingContainer extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      calculatedWidth: props.item.amount.toString().length * 18 + 20
    }

    this.inputRef = React.createRef()

    this.onUpdate = this.onUpdate.bind(this)
    this.handleChange = this.handleChange.bind(this)
  }

  handleSubmit(event) {
    event.preventDefault()
    if (document && document.activeElement) document.activeElement.blur()
  }

  onUpdate(event) {
    let {
      updateTraining,
      item
    } = this.props

    updateTraining({
      id: item.id,
      amount: event.target.value
    })
  }

  handleChange() {
    this.setState({
      calculatedWidth: this.inputRef.current.value.toString().length * 18 + 20
    })
  }

  render() {
    return(
      <Training { ...this.props } onChange={this.handleChange} inputRef={this.inputRef} onUpdate={ this.onUpdate } onSubmit={ this.handleSubmit } calculatedWidth={this.state.calculatedWidth} />
    )
  }
}

const mapDispatchesToProps = (dispatch, { item }) => ({
  updateTraining(training) {
    dispatch(updateTraining(training))
  },
  onDeleteTraining() {
    dispatch(deleteTraining(item.id))
  }
})

export default withRouter(
  connect(
    null,
    mapDispatchesToProps
  )(TrainingContainer)
)