import styled from 'styled-components'

const StyledTraining = styled.form`
  display: flex;
  align-items: center;
  margin: .6rem 0;

  label {
    font-size: 1.1em;
    margin-right: 8px;
  }

  input {
    font-size: 2em;
    font-family: 'Muli';
    font-weight: bold;
    border: none;
    background: transparent;
    text-align: right;

    &:hover {
      outline: #ccc auto 5px;
    }

    &:active,
    &:focus {
      outline: focus-ring-color auto 5px;
      outline: -webkit-focus-ring-color auto 5px;
      outline: -moz-focus-ring-color auto 5px;
    }
  }

  .delete-icon {
    font-size: 1.1em
    margin-left: auto;
    cursor: pointer;
    transition: color .3s ease-out;

    &:hover {
      color: red;
    }
  }
`

export default StyledTraining