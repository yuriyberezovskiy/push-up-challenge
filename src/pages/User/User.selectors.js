import { createSelector } from 'reselect'

export const getSelectedUser = (state) => state.user && state.user.selected

export const getMaxPushUpsFromDay = (selected = {}) => {
  if (!selected.days) return 0;
  
  let max = 0;

  selected.days.forEach(day => {
    if (day.push_ups > max) {
      max = day.push_ups;
    }
  })

  return max
}

export const getMaxPushUpsFromTrainings = (selected = {}) => {
  if (!selected.days) return 0;
  
  let max = 0;

  selected.days.forEach(day => {
    day.trainings.forEach(training => {
      if (training.amount > max) {
        max = training.amount;
      }
    })
  })

  return max
}
export const getAmountUserPushUps = createSelector(
  [getSelectedUser],
  (selected) => selected && selected.days ? selected.days.reduce((acc, item) => (acc + item.push_ups), 0) : 0
)