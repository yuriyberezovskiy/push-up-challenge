import { call, put, takeEvery } from 'redux-saga/effects'
import Api from '../../common/Api/Api'
import {
  FETCH_USER,
  fetchUserSuccess,
  fetchUserError,
  addTrainingSuccess,
  addTrainingError,
  ADD_TRAINING,
  deleteUserSuccess,
  deleteUserError,
  DELETE_USER,
  UPDATE_TRAINING,
  updateTrainingSuccess,
  updateTrainingError,
  DELETE_TRAINING,
  deleteTrainingSuccess,
  deleteTrainingError,
  UPDATE_USER,
  updateUserSuccess,
  updateUserError
} from './User.actions'

/**
 * Start Load user
 */
function loadUser(id) {
  return Api.get('user', {
    params: {
      user: id
    }
  })
  .then(({data}) => data)
}

function* getUser({id}) {
  try {
    const user = yield call(loadUser, id)
    yield put(fetchUserSuccess(user))
  } catch (error) {
    yield put(fetchUserError(error))
  }
}
/**
 * End Load user
 */

/**
 * Start Update user
 */
function onUpdateUser(action) {
  let { id, ...rest } = action.payload

  return Api.patch(`users/${id}`, {
    ...rest
  })
    .then(({data}) => data)
}

function* handleUpdateUser(payload) {
  try {
    const user = yield call(onUpdateUser, payload)
    yield put(updateUserSuccess(user))
  } catch (error) {
    yield put(updateUserError(error))
  }
}
/**
 * End Load user
 */

/**
 * Start Delete user
 */
function onDeleteUser(id) {
  return Api.delete(`users/${id}`)
    .then(({data}) => data)
}

function* handleDeleteUser({id}) {
  try {
    const user = yield call(onDeleteUser, id)
    yield put(deleteUserSuccess({
      id,
      ...user
    }))
  } catch (error) {
    yield put(deleteUserError(error))
  }
}
/**
 * End Load user
 */

/**
 * Start Add trainings
 */
function onAddTraining(payload) {
  return Api.post('trainings', {
    ...payload
  })
    .then(({data}) => data)
}

 function* handleAddTraining(action) {
   try {
    const training = yield call(onAddTraining, action.payload)
    yield put(addTrainingSuccess(training))
   } catch (error) {
    yield put(addTrainingError(error))
   }
 }
/**
 * End Add trainings
 */

/**
 * Start delete training
 */
function onDeleteTraining(payload) {
  return Api.delete(`trainings/${payload}`)
    .then(({data}) => data)
}

 function* handleDeleteTraining(action) {
   try {
    const training = yield call(onDeleteTraining, action.payload)
    yield put(deleteTrainingSuccess({
      id: action.payload,
      ...training
    }))
   } catch (error) {
    yield put(deleteTrainingError(error))
   }
 }
/**
 * End Delete training
 */

/**
 * Start Update training
 */
function onUpdateTraining(payload) {
  return Api.patch(`trainings/${payload.id}`, {
    amount: payload.amount
  })
    .then(({data}) => data)
}

 function* handleUpdateTraining(action) {
   try {
    const training = yield call(onUpdateTraining, action.payload)
    yield put(updateTrainingSuccess(training))
   } catch (error) {
    yield put(updateTrainingError(error))
   }
 }
/**
 * End Update training
 */

export default [
  takeEvery(FETCH_USER, getUser),
  takeEvery(ADD_TRAINING, handleAddTraining),
  takeEvery(DELETE_USER, handleDeleteUser),
  takeEvery(UPDATE_TRAINING, handleUpdateTraining),
  takeEvery(DELETE_TRAINING, handleDeleteTraining),
  takeEvery(UPDATE_USER, handleUpdateUser),
]