import React from 'react';
import moment from 'moment'
import { Link } from 'react-router-dom';
import { Form, Grid, Progress, Table, Modal, Input, Button, Message, Loader, Card, Feed, Image, Item, Popup, Dimmer } from 'semantic-ui-react';
import viking from '../../viking.svg'
import { START_DATE } from '../../config/dates'
import Training from './Training/Training.container'
import CollapsedBox from '../../common/components/CollapsedBox/CollapsedBox.container'
import TextEdit from '../../common/components/TextEdit/TextEdit.container'

const User = ({ checkAccessRights, loadingForDay, pushUps, patchUser, selected, isModalOpen, onCloseModal, onSelectDay, onAddTraining, onChanged, loading, error, maxPushUpsPerTraining, maxPushUpsPerDay, maxPushUpsForPeriod, amountPushUps, calcUserPower, powerStatus, selectedDay }) => (
  <React.Fragment>
    {loading ? (
      <Loader active inline='centered'>Loading...</Loader>
    ) : (
    <React.Fragment>
      <Link to='/' style={{marginBottom: '20px', display: 'inline-block'}}>
        <Button color='black' title='Back to home page' circular icon='left arrow' />
      </Link>

      <Grid>
        <Grid.Row>
          <Grid.Column width='3'>
            <Card>
              <Card.Content>
                <Card.Header>Push Up Activity!</Card.Header>
              </Card.Content>
              <Image src={`https://react.semantic-ui.com/images/avatar/large/${['matthew.png', 'elliot.jpg', 'steve.jpg', 'daniel.jpg'][Math.floor(Math.random() * 4)]}`} />
              <Card.Content>
                <Card.Header>
                  {checkAccessRights() ? (
                    <TextEdit style={{
                      fontWeight: 700,
                      fontFamily: 'Lato,"Helvetica Neue",Arial,Helvetica,sans-serif',
                      color: 'rgba(0,0,0,.85)'
                      }} value={selected.username} name='username' onUpdate={patchUser} />
                  ) : selected.username}
                </Card.Header>
                <Card.Meta>
                  {checkAccessRights() ? (
                    <TextEdit style={{ color: 'rgba(0,0,0,.4)' }} value={selected.email} name='email' onUpdate={patchUser} />
                  ) : selected.email}
                </Card.Meta>
                <Feed>
                  <Feed.Content>
                    <Feed.Summary>
                      <Feed.Label>
                        Best training: <strong>{ maxPushUpsPerTraining }</strong>
                      </Feed.Label>
                      <Feed.Label>
                        Best day: <strong>{ maxPushUpsPerDay }</strong>
                      </Feed.Label>
                    </Feed.Summary>
                  </Feed.Content>
                </Feed>
              </Card.Content>
            </Card>
          </Grid.Column>

          <Grid.Column width='13' verticalAlign='top'>
            <Item.Group>
              <Item>
                <Item.Image size='tiny' src={viking} />

                <Item.Content>
                  <Item.Header as='p'>Hello my friend!</Item.Header>
                  <Item.Description style={{fontSize: '1.1em'}}>
                    I'm Norseman, I can determine participant power!
                  </Item.Description>
                  <Button onClick={calcUserPower} color='blue' style={{marginTop: '20px'}}>Ask Norseman!</Button>
                  <Item.Meta style={{fontWeight: 'bold', fontSize: '1.2em'}}>{ powerStatus }</Item.Meta>
                </Item.Content>
              </Item>
            </Item.Group>

            <Progress disabled={!maxPushUpsForPeriod} value={!maxPushUpsForPeriod ? 0 : amountPushUps} total={maxPushUpsForPeriod} progress={!maxPushUpsForPeriod ? false : 'percent'} success active />

            <p>
              <strong>Table results</strong>
            </p>
            <Table celled>
              <Table.Header>
                <Table.Row>
                  {selected.days && selected.days.length ? selected.days.map((day, index) => (
                    <Popup key={day.day_number}
                      trigger={
                        <Table.HeaderCell key={day.day_number} className={ day.is_current ? 'is-current' : null}>
                          {day.day_number}
                        </Table.HeaderCell>
                      }
                      content={moment(START_DATE, 'L').add(index, 'days').format('MMM Do YYYY')}
                    />
                  )) : null}
                </Table.Row>
              </Table.Header>

              <Table.Body>
                <Table.Row>
                  {selected.days && selected.days.length ? selected.days.map(day => (
                    <Table.Cell key={day.day_number} verticalAlign="top"
                    className={ day.is_current ? 'is-current' : day.push_ups >= 100 ? 'highlight' : null}
                      onClick={() => {onSelectDay(day)}}>
                      {day.push_ups}

                      <ul className="training-list">
                        {day.trainings.length ? (
                            day.trainings.map((training, index) => (
                              <div key={ index }>{training.amount}</div>
                            ))
                        ) : null}
                      </ul>
                    </Table.Cell>
                  )) : null}
                </Table.Row>
              </Table.Body>
            </Table>

            <Modal size='tiny' open={isModalOpen} onClose={onCloseModal}>
              <Modal.Header>Add Push Ups!</Modal.Header>
              {!loadingForDay ? null : (
                <Dimmer active inverted>
                  <Loader inverted inline='centered'>
                    Loading...
                  </Loader>
                </Dimmer>
              )}
              <Modal.Content>
                <section>
                  <Form onSubmit={onAddTraining}>
                    <Input autoFocus fluid name='pushUps' placeholder='0' value={pushUps} type='number' max="200" min="1" onChange={onChanged} />
                    {error && (<Message
                      header='Что-то пошло не так!'
                      content='Проверьте количество введённых отжиманий или обратитесь к администратору!'
                    />)}
                  </Form>
                </section>
                <CollapsedBox header='Daily Trainings'>
                  {!selectedDay || !selectedDay.trainings ? null : (
                    selectedDay.trainings.map((item, index) => (
                      <Training key={ item.id } item={ item } index={ index } />
                    ))
                  )}
                </CollapsedBox>
              </Modal.Content>
              <Modal.Actions>
                <Button onClick={onCloseModal} negative>Close</Button>
                {!pushUps ? null : (
                  <Button loading={loading} disabled={loading} onClick={onAddTraining} positive>
                    Ok
                  </Button>
                )}
              </Modal.Actions>
            </Modal>
          </Grid.Column>
        </Grid.Row>
      </Grid>
    </React.Fragment>
    )}
  </React.Fragment>
);

export default User;