export const FETCH_USER = 'FETCH_USER'
export const FETCH_USER_SUCCESS = 'FETCH_USER_SUCCESS'
export const FETCH_USER_ERROR = 'FETCH_USER_ERROR'

export const DELETE_USER = 'DELETE_USER'
export const DELETE_USER_SUCCESS = 'DELETE_USER_SUCCESS'
export const DELETE_USER_ERROR = 'DELETE_USER_ERROR'

export const UPDATE_USER = 'UPDATE_USER'
export const UPDATE_USER_SUCCESS = 'UPDATE_USER_SUCCESS'
export const UPDATE_USER_ERROR = 'UPDATE_USER_ERROR'

export const ADD_TRAINING = 'ADD_TRAINING'
export const ADD_TRAINING_SUCCESS = 'ADD_TRAINING_SUCCESS'
export const ADD_TRAINING_ERROR = 'ADD_TRAINING_ERROR'

export const UPDATE_TRAINING = 'UPDATE_TRAINING'
export const UPDATE_TRAINING_SUCCESS = 'UPDATE_TRAINING_SUCCESS'
export const UPDATE_TRAINING_ERROR = 'UPDATE_TRAINING_ERROR'

export const DELETE_TRAINING = 'DELETE_TRAINING'
export const DELETE_TRAINING_SUCCESS = 'DELETE_TRAINING_SUCCESS'
export const DELETE_TRAINING_ERROR = 'DELETE_TRAINING_ERROR'

export const SELECT_DAY = 'SELECT_DAY'

export const selectDay = (payload) => ({
  type: SELECT_DAY,
  payload
})

export const fetchUser = (id) => ({
  type: FETCH_USER,
  id
})

export const fetchUserSuccess = (payload) => ({
  type: FETCH_USER_SUCCESS,
  payload
})

export const fetchUserError = (err) => ({
  type: FETCH_USER_SUCCESS,
  payload: err
})

export const deleteUser = (id) => ({
  type: DELETE_USER,
  id
})

export const deleteUserSuccess = (payload) => ({
  type: DELETE_USER_SUCCESS,
  payload
})

export const deleteUserError = (err) => ({
  type: DELETE_USER_SUCCESS,
  payload: err
})

export const updateUser = (payload) => ({
  type: UPDATE_USER,
  payload
})

export const updateUserSuccess = (payload) => ({
  type: UPDATE_USER_SUCCESS,
  payload
})

export const updateUserError = (payload) => ({
  type: UPDATE_USER_ERROR,
  payload
})

export const addTraining = (payload) => ({
  type: ADD_TRAINING,
  payload
})

export const addTrainingSuccess = (payload) => ({
  type: ADD_TRAINING_SUCCESS,
  payload
})

export const addTrainingError = (err) => ({
  type: ADD_TRAINING_ERROR,
  payload: err
})

export const updateTraining = (payload) => ({
  type: UPDATE_TRAINING,
  payload
})

export const updateTrainingSuccess = (payload) => ({
  type: UPDATE_TRAINING_SUCCESS,
  payload
})

export const updateTrainingError = (error) => ({
  type: UPDATE_TRAINING_ERROR,
  error
})

export const deleteTraining = (payload) => ({
  type: DELETE_TRAINING,
  payload
})

export const deleteTrainingSuccess = (payload) => ({
  type: DELETE_TRAINING_SUCCESS,
  payload
})

export const deleteTrainingError = (payload) => ({
  type: DELETE_TRAINING_ERROR,
  payload
})