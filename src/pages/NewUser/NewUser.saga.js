import { call, put, takeEvery } from 'redux-saga/effects'
import Api from '../../common/Api/Api'
import {
  createUserSuccess,
  createUserError,
  CREATE_USER
} from './NewUser.actions';

/**
 * Start Create user
 */

function onCreateUser(payload) {
  return Api.post('users', {
    username: payload.username,
    email: payload.email
  })
  .then(({data}) => data)
}

function* handleCreateUser(action) {
  try {
    const user = yield call(onCreateUser, action.payload)
    yield put(createUserSuccess(user))
  } catch (error) {
    yield put(createUserError(error))
  }
}

/**
 * End Create user
 */

export default [
  takeEvery(CREATE_USER, handleCreateUser)
]