import React from 'react';
import { Form, Grid, Header, Button } from 'semantic-ui-react';
import { Link } from 'react-router-dom';

const NewUser = ({ username, email, onChange, onSubmit, loading }) => (
  <Grid centered columns={1} textAlign='center'>

    <Grid.Row>
      <Grid.Column style={{ maxWidth: 450 }}>
        <Link to='/'>
          <Button color='black' title='back to home page' circular icon='left arrow' />
        </Link>

        <Header as='h3' textAlign='center'>
          New participant!
        </Header>

        <Form onSubmit={onSubmit}>
          <Form.Input type="text" placeholder="Имя" name='username' value={username} onChange={onChange} />
          <Form.Input type="text" placeholder="Email" name='email' value={email} onChange={onChange} />

          <Form.Button loading={loading} type="submit" secondary style={{ width: '100%' }}>
            Add
          </Form.Button>
        </Form>
      </Grid.Column>
    </Grid.Row>
  </Grid>
);

export default NewUser;