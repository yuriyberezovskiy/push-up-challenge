import React, { Component } from 'react'
import { connect } from 'react-redux'
import NewUser from './NewUser.component'
import withAccessRights from '../../common/AccessRights/withAccessRights'
import { createUser } from './NewUser.actions';

class NewUserContainer extends Component {
  constructor(props) {
    super(props);

    this.state = {
      username: '',
      email: '',
      showSuccess: {
        success: false,
        error: false
      }
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(e, { name, value }) {
    this.setState({
      [name]: value
    })
  }

  handleSubmit() {
    let { username, email } = this.state

    this.props.onCreateUser({
      username,
      email
    })

    this.setState({
      username: '',
      email: ''
    })
  }

  render() {
    let {
      email,
      username,
      showSuccess
    } = this.state

    return (
      <NewUser { ...this.props }
        showSuccess={ showSuccess }
        onChange={this.handleChange}
        onSubmit={ this.handleSubmit }
        username={username}
        email={email} />
    )
  }
}

const mapStateToProps = ({users: { loading }}) => ({
  loading
})

const mapDispatchesToProps = (dispatch) => ({
  onCreateUser(data) {
    dispatch(createUser(data))
  }
})

export default withAccessRights(
  connect(
    mapStateToProps,
    mapDispatchesToProps
  )(NewUserContainer)
  
);