export const CREATE_USER = 'CREATE_USER'
export const CREATE_USER_SUCCESS = 'CREATE_USER_SUCCESS'
export const CREATE_USER_ERROR = 'CREATE_USER_ERROR'

export const createUser = (payload) => ({
  type: CREATE_USER,
  payload
})

export const createUserSuccess = (payload) => ({
  type: CREATE_USER_SUCCESS,
  payload
})

export const createUserError = (payload) => ({
  type: CREATE_USER_ERROR,
  payload
})