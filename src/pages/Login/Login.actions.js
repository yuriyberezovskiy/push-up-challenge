export const LOGIN = 'LOGIN'
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS'
export const LOGIN_ERROR = 'LOGIN_ERROR'

export const CHECK_LOGIN = 'CHECK_LOGIN'
export const CHECK_LOGIN_SUCCESS = 'CHECK_LOGIN_SUCCESS'
export const CHECK_LOGIN_ERROR = 'CHECK_LOGIN_ERROR'

export const LOGOUT = 'LOGOUT'

export const logout = () => ({
  type: LOGOUT
})

export const login = (payload) => ({
  type: LOGIN,
  payload
})

export const loginSuccess = (payload) => ({
  type: LOGIN_SUCCESS,
  payload
})

export const loginError = (err) => ({
  type: LOGIN_ERROR,
  payload: err
})

export const checkLogin = () => ({
  type: CHECK_LOGIN
})

export const checkLoginSuccess = (payload) => ({
  type: CHECK_LOGIN_SUCCESS,
  payload
})

export const checkLoginError = (err) => ({
  type: CHECK_LOGIN_ERROR,
  payload: err
})