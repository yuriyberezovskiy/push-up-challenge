import React from 'react'
import { connect } from 'react-redux'
import Login from './Login.component'
import { login } from './Login.actions'
import { replace } from 'react-router-redux'

class LoginContainer extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      username: '',
      password: ''
    }

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentDidMount() {
    let { login, redirectTo } = this.props

    if (login.loggedIn) {
      redirectTo('/')
    }
  }

  static getDerivedStateFromProps(nextProps) {
    if (nextProps.login.loggedIn) {
      nextProps.redirectTo('/')
    }
    return null
  }

  handleChange(e, {name, value}) {
    this.setState({
      [name]: value
    });
  }

  handleSubmit(e) {
    e.preventDefault();

    let { username, password } = this.state;

    this.props.onLogin({
      username,
      password
    })
  }

  render() {
    let {
      username,
      password
    } = this.state;

    return (
      <Login {...this.props } username={username} password={password} onChange={this.handleChange} onSubmit={this.handleSubmit} />
    )
  }
}

const mapStateToProps = ({ login }) => ({
  login: login.data,
  loading: login.loading
})

const mapDispatchesToProps = (dispatch) => ({
  onLogin(data) {
    dispatch(login(data))
  },
  redirectTo(path) {
    dispatch(replace(path))
  }
})

export default connect(
  mapStateToProps,
  mapDispatchesToProps
)(LoginContainer)