import { call, put, takeEvery } from 'redux-saga/effects'
import Api from '../../common/Api/Api'
import Cookies from 'js-cookie'
import { cookies as cookiesConfig } from '../../config/login'
import {
  LOGIN,
  loginSuccess,
  loginError,
  CHECK_LOGIN,
  checkLoginSuccess,
  checkLoginError,

} from './Login.actions'

function onLogin(payload) {
  return Api.post('login', payload)
    .then(({ data }) => data)
}

function* handleLogin(action) {
  try {
    const response = yield call(onLogin, action.payload)

    Cookies.set(
      cookiesConfig.token.name,
      response.api_key
    );

    yield put(loginSuccess(response))
  } catch ( error ) {
    yield put(loginError(error))
  }
}

function checkLogin() {
  return Api.get('currentuser')
    .then(({ data }) => data)
}

function* handleCheckLogin() {
  try {
    let token = Cookies.get(cookiesConfig.token.name)

    if (token) {
      let loginInfo = yield call(checkLogin)
      yield put(checkLoginSuccess(loginInfo))
    } else {
      yield put(checkLoginError())
    }

  } catch (error) {
    yield put(checkLoginError(error))
  }
}

export default [
  takeEvery(LOGIN, handleLogin),
  takeEvery(CHECK_LOGIN, handleCheckLogin)
]