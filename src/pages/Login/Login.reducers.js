import { combineReducers } from 'redux'
import {
  LOGIN,
  LOGIN_SUCCESS,
  LOGIN_ERROR,
  CHECK_LOGIN,
  CHECK_LOGIN_SUCCESS,
  CHECK_LOGIN_ERROR,
  LOGOUT
} from './Login.actions'

const loading = (state = false, action) => {
  switch(action.type) {
    case LOGIN:
      return true
    case LOGIN_SUCCESS:
    case LOGIN_ERROR:
      return false
    default:
      return state
  }
}

const initialState = {
  loggedIn: false
}

const data = (state = initialState, action) => {
  switch(action.type) {
    case CHECK_LOGIN:
      return {
        loggedIn: CHECK_LOGIN
      }
    case LOGIN_SUCCESS:
    case CHECK_LOGIN_SUCCESS:
      return {
        ...action.payload,
        loggedIn: true
      }
    case LOGOUT:
    case LOGIN_ERROR:
    case CHECK_LOGIN_ERROR:
      return {
        loggedIn: false
      }
    default:
      return state
  }
}

export default combineReducers({
  data,
  loading
})