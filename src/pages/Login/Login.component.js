import React from 'react';
import StyledLogin from './Login.styled';
import { Form, Grid, Message, Image } from 'semantic-ui-react';

const Login = ({ username, password, onChange, onSubmit, error, loading }) => (
  <StyledLogin>
    <Grid centered columns={1} textAlign='center'>
      <Grid.Row>
        <Grid.Column style={{ width: 450 }}>
          <p>
            <Image centered src='http://norse.co/images/assets/svg/icons/logo.svg' />
          </p>

          <Form onSubmit={onSubmit}>
            <Form.Input type="text" placeholder="Username" name='username' value={username} onChange={onChange} />
            <Form.Input type="password" placeholder="********" name='password' value={password} onChange={onChange} />

            {error && (<Message
              header='Incorect data'
              content='Check fields `Username` and `Password`, it seems something wrong!'
            />)}
            <Form.Button loading={loading} type="submit" secondary style={{ width: '100%' }}>
                Login
              </Form.Button>
          </Form>
        </Grid.Column>
      </Grid.Row>
    </Grid>
  </StyledLogin>
);

export default Login