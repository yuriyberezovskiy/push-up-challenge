import styled from 'styled-components'

const StyledLogin = styled.div`
  height: inherit;
  min-height: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
`

export default StyledLogin