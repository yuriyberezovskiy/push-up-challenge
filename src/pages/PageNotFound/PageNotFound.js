import React from 'react';
import { Header } from 'semantic-ui-react';
import { Link } from 'react-router-dom';

const PageNotFound = () => (
  <div style={{'textAlign': 'center'}}>
    <Header as='h3' textAlign='center'>
      <strong>404</strong> Страница не найдена!
    </Header>
    <Link to='/'>
      На главную
    </Link>
  </div>
);

export default PageNotFound;