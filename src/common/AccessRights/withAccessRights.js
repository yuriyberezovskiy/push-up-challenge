import React from 'react';
import Cookies from 'js-cookie';
import { cookies as cookiesConfig } from '../../config/login';

function withAccessRights(Component) {

  class Wrapper extends React.Component {
    checkAccessRights() {
      return Cookies.get(cookiesConfig.token.name);
    }

    render() {
      return (
        <Component {...this.props} checkAccessRights={this.checkAccessRights} />
      );
    }
  }

  return Wrapper;
}

export default withAccessRights;