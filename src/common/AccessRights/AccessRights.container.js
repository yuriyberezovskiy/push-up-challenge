import React from 'react';
import withAccessRights from './withAccessRights';

class AccessRightsContainer extends React.Component {
  render() {
    return (
      this.props.checkAccessRights() ? this.props.children : null
    )
  }
}

export default withAccessRights(
  AccessRightsContainer
);