import React from 'react'
import { Message, Icon } from 'semantic-ui-react'

const Error = ({ message, onDelete, index }) => (
  <Message negative>
    <Icon className='remove-btn' name='delete' onClick={() => {onDelete(index)}} />
    <Message.Header>Error</Message.Header>
    <p>{message}</p>
  </Message>
)

export default Error