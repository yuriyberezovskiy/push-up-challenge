import styled from 'styled-components'

const Errors = styled.div`
  position: fixed;
  right: 16px;
  bottom: 16px;
  width: 240px;

  .remove-btn {
    cursor: pointer;
    position: absolute;
    right: 0px;
    top: 6px;
  }
`

export default Errors