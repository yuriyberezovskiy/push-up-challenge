import React from 'react'
import { connect } from 'react-redux'
import StyledErrors from './Errors.styled'
import Error from './Errors.component'
import { removeError } from './Errors.actions'

class Errors extends React.Component {
  render() {
    let { errors } = this.props

    return (
        <StyledErrors>
          {errors.map((item, index) => (
            <Error key={index} message={item.message} onDelete={this.props.removeError} index={index} />
          ))}
        </StyledErrors>
      )
  }
}

const mapStateToProps = ({ errors }) => ({
  errors
})

const mapDispatchesToProps = (dispatch) => ({
  removeError(index) {
    dispatch(removeError(index))
  }
})

export default connect(
  mapStateToProps,
  mapDispatchesToProps
)(Errors)