import React from 'react'
import { Button } from 'semantic-ui-react'

const Confirm = ({ onClose, onConfirm, title, content }) => (
  <div className='custom-ui'>
    <h1>{ title }</h1>
    <p>{ content }</p>
    <Button onClick={onClose} negative>Cancel</Button>
    <Button onClick={() => {
        onConfirm()
        onClose()
    }} positive>Ok</Button>
  </div>
)

Confirm.defaultProps = {
  title: 'Are you sure?'
}

export default Confirm