import styled from 'styled-components'

const StyledTextEdit = styled.div`
  display: block;

  input {
    cursor: pointer;
    max-width: 100%;
    overflow: hidden;
    margin: 0;
    padding: 2px 0;
    text-indent: 4px;
    border: none;
    background: transparent;

    &:hover {
      outline: #ccc auto 5px;
    }

    &:active,
    &:focus {
      outline: focus-ring-color auto 5px;
      outline: -webkit-focus-ring-color auto 5px;
      outline: -moz-focus-ring-color auto 5px;
    }
  }

  .icon {
    cursor: pointer;
  }
`

export default StyledTextEdit