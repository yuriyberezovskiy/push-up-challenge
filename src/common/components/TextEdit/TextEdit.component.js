import React from 'react'
import StyledTextEdit from './TextEdit.styled'
import { Icon } from 'semantic-ui-react'

const TextEdit = ({ value, onChanged, name, onBlur, focusTextInput, textInput, style }) => (
  <StyledTextEdit>
    <input style={style} ref={textInput} type='text' size={value && value.length} value={value} name={name} onChange={onChanged} onBlur={onBlur} />
    <Icon className='icon' name='pencil' onClick={focusTextInput} />
  </StyledTextEdit>
)

export default TextEdit