import React from 'react'
import TextEdit from './TextEdit.component'

class TextEditContainer extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      [this.props.name]: this.props.value,
    }

    this.textInput = React.createRef()
    this.onChanged = this.onChanged.bind(this)
    this.onBlur = this.onBlur.bind(this)
    this.focusTextInput = this.focusTextInput.bind(this)
  }

  focusTextInput() {
    this.textInput.current.focus();
  }

  onChanged(event) {
    this.setState({
      [event.target.name]: event.target.value
    });
  }

  onBlur() {
    this.props.onUpdate({
      ...this.state
    })
  }

  render() {
    return (
      <TextEdit {...this.props}
        textInput={this.textInput}
        focusTextInput={this.focusTextInput}
        value={ this.state[this.props.name] }
        onChanged={this.onChanged}
        onBlur={this.onBlur} />
    )
  }
}

export default TextEditContainer