import styled from 'styled-components'

const StyledFooter = styled.footer`
  padding: 24px 0;
  margin: 48px 0 0 0;
  border-top: 1px solid rgba(34,36,38,.15);
`

export default  StyledFooter