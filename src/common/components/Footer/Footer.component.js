import React from 'react'
import StyledFooter from './Footer.styled'
import { Container, Grid } from 'semantic-ui-react'

const Footer = () => (
  <StyledFooter>
    <Container>
      <Grid>
        <Grid.Row>
          <Grid.Column textAlign='center'>
            <p>© 2018 NORSE · All rights reserved</p>
          </Grid.Column>
        </Grid.Row>
      </Grid>
    </Container>
  </StyledFooter>
)

export default Footer