import React from 'react';
import CollapsedBox from './CollapsedBox.component'

class CollapsedBoxContainer extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      collapsed: this.props.collapsed
    }

    this.handleToggle = this.handleToggle.bind(this)
  }

  handleToggle() {
    this.setState(prevState => ({
      collapsed: !prevState.collapsed
    }))
  }

  render() {
    return(
      <CollapsedBox { ...this.props } collapsed={ this.state.collapsed } onToggle={ this.handleToggle } />
    )
  }
}

CollapsedBoxContainer.defaultProps = {
  collapsed: false
}

export default CollapsedBoxContainer