import React from 'react'
import StyledCollapsed from './CollapsedBox.styled'
import { Icon } from 'semantic-ui-react'

const CollapsedBox = ({ collapsed, onToggle, children, header }) => (
  <StyledCollapsed>
    <header>
      <span onClick={onToggle}>
        { header }
        <Icon name={ collapsed ? 'angle down' : 'angle up' } />
      </span>
    </header>
    {collapsed ? null : (
      <div>
        {children}
      </div>
    )}
  </StyledCollapsed>
)

export default CollapsedBox