import styled from 'styled-components'

const StyledCollapsedBox = styled.div`
  margin: 1rem .1rem;
  border: 1px solid #e6e6e6;
  padding: 2rem;
  border-radius: 4px;

  header {
    font-size: 1.2em;
    font-weight: bold;

    span {
      cursor: pointer
    }
  }
`

export default StyledCollapsedBox