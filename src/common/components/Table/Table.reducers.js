import { combineReducers } from 'redux'
import {
  UPDATE_TABLE_FILTER
} from './Table.actions'

const filter = (state = '', action) => {
  switch(action.type) {
    case UPDATE_TABLE_FILTER:
      return action.payload
    default:
      return state
  }
}

export default combineReducers({
  filter
})