export const UPDATE_TABLE_FILTER = 'UPDATE_TABLE_FILTER'

export const updateTableFilter = (text) => ({
  type: UPDATE_TABLE_FILTER,
  payload: text
})