import styled from 'styled-components'

const StyledFooter = styled.header`
  padding: 24px 0;
  margin: 0 0 48px 0;
  border-bottom: 1px solid rgba(34,36,38,.15);

  .selected .ui.basic.button {
    background: #fff!important;
    color: rgba(0,0,0,.8)!important;
    -webkit-box-shadow: 0 0 0 1px rgba(34,36,38,.35) inset, 0 0 0 0 rgba(34,36,38,.15) inset;
    box-shadow: 0 0 0 1px rgba(34,36,38,.35) inset, 0 0 0 0 rgba(34,36,38,.15) inset;
  }

  .user-data {
    position: absolute;
    left: 20px;
    top: 16px;
    font-size: 1.2em;
  }

  .button-right {
    position: absolute;
    right: 0;
    top: 0;
  }
`

export default  StyledFooter