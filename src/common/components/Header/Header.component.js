import React from 'react'
import StyledHeader from './Header.styled'
import { Container, Grid, Button } from 'semantic-ui-react'
import { NavLink, Link } from 'react-router-dom'

const Header = ({ username, loggedIn }) => (
  <StyledHeader>
    <Container style={{width: '1480px'}}>
      <Grid>
        <Grid.Row>
          <Grid.Column textAlign='center'>
            <div className='user-data'>
              {!loggedIn ? null : (
                  <span>Hello, <u>{ username }!</u></span>
              )}
            </div>
            <NavLink to='/voting' className='button-right' activeClassName='selected'>
              <Button size='big' basic>
                Make your prediction!
              </Button>
            </NavLink>

            <h2 style={{marginTop: '9px'}}>
              <Link to='/'>
                &lt;Push Up Challenge 2018/&gt;
              </Link>
            </h2>
          </Grid.Column>
        </Grid.Row>
      </Grid>
    </Container>
  </StyledHeader>
)

export default Header