import React from 'react'
import { connect } from 'react-redux'
import Header from './Header.component'

const getUserName = (state) => state.login && state.login.data && state.login.data.username

class HeaderContainer extends React.Component {

  render() {
    return (
      <Header {...this.props} />
    )
  }
}

const mapStateToProps = (state) => ({
  username: getUserName(state),
  loggedIn: state.login.data.loggedIn
})

export default connect(
  mapStateToProps
)(HeaderContainer)