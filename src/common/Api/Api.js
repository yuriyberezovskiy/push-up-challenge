import axios from 'axios'
import { logout, CHECK_LOGIN } from '../../pages/Login/Login.actions'
import { showError } from '../Dialogs/Errors/Errors.actions'
import { cookies as cookiesConfig } from '../../config/login'
import { replace } from 'react-router-redux'
import Cookies from 'js-cookie'

const Api = axios.create({
  baseURL: `http://68.168.210.108/api/`
});

const createUpdateAuthInterceptor = (store, http) => error => {
  if (error.response.status === 403 || error.response.status === 401) {
    Cookies.remove(cookiesConfig.token.name)
    store.dispatch(logout())
    store.dispatch(replace('/admin'))
  }

  store.dispatch(showError({
    message: error.response.data.message
  }))

  return Promise.reject(error);
}

const createSetTokenInterceptor = ({ getState }) => config => {
  let state = getState()
  let { loggedIn, api_key } = state.login.data

  if (loggedIn === CHECK_LOGIN) {
    let token = Cookies.get(cookiesConfig.token.name)
    config.headers.common[cookiesConfig.token.name] = token
  } else if (loggedIn) {
    config.headers.common[cookiesConfig.token.name] = api_key
  } else {
    delete config.headers.common[cookiesConfig.token.name]
  }

  return config
}

export const setupApiInterceptors = store => {
  const setToken = createSetTokenInterceptor(store)
  Api.interceptors.request.use(setToken)

  const updateAuthCb = createUpdateAuthInterceptor(store, axios)
  Api.interceptors.response.use(null, updateAuthCb)
}

export default Api