import { createStore, applyMiddleware, combineReducers } from 'redux';
import initialReducers from './initialReducers'
import { composeWithDevTools } from 'redux-devtools-extension'
import thunkMiddleware from 'redux-thunk'
import loggerMiddleware from './middleware/logger'

import { all } from 'redux-saga/effects'
import loginSaga from '../pages/Login/Login.saga'
import homeSaga from '../pages/Home/Home.saga'
import userSaga from '../pages/User/User.saga'
import newUserSaga from '../pages/NewUser/NewUser.saga'

import monitorReducerEnhancer from './enhancers/monitorReducer'
import moment from 'moment'
import sagaMiddleware from './middleware/sagaMiddleware'
import { connectRouter, routerMiddleware } from 'connected-react-router'
import history from './history'
import { START_DATE } from '../config/dates'
import { setupApiInterceptors } from '../common/Api/Api';

export default function configureStore() {
  const initialState = {
    login: {
      data: {
        loggedIn: false
      }
    },
    days: Array.from({length: 30}).map((_, index) => ({
      day: index+1,
      hidden: moment().subtract(index, 'day') <= moment(START_DATE, 'L'),
      isCurrent: moment().subtract(index, 'days').format('L') === START_DATE
    }))
  }

  const middleware = [loggerMiddleware, thunkMiddleware, sagaMiddleware]
  const middlewareEnhancer = applyMiddleware(...middleware, routerMiddleware(history))

  const enhancers = [middlewareEnhancer, monitorReducerEnhancer]
  const composedEnhancers = composeWithDevTools(...enhancers)

  const store = createStore(
    connectRouter(history)(combineReducers(initialReducers)),
    initialState,
    composedEnhancers
  )

  setupApiInterceptors(store)

  sagaMiddleware.run(
    function* () {
      yield all([
        ...loginSaga,
        ...homeSaga,
        ...userSaga,
        ...newUserSaga
      ])
    }
  )

  if (process.env.NODE_ENV !== 'production' && module.hot) {
    module.hot.accept('./initialReducers', () =>
      store.replaceReducer(initialReducers)
    )
  }

  return store
}
