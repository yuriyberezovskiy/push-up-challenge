import TableReducer from '../common/components/Table/Table.reducers'
import ErrorsReducer from '../common/Dialogs/Errors/Errors.reducers'
import LoginReducer from '../pages/Login/Login.reducers'
import UsersReducer from '../pages/Home/Home.reducers'
import UserReducer from '../pages/User/User.reducers'

export default {
  login: LoginReducer,
  users: UsersReducer,
  user: UserReducer,
  days: (state = []) => state,
  errors: ErrorsReducer,
  table: TableReducer
}

