import React, { Component } from 'react'
import { connect } from 'react-redux'
import 'semantic-ui-css/semantic.min.css'
import './App.css'
import { Route, Switch } from 'react-router'
import { Container } from 'semantic-ui-react'
import Home from './pages/Home/Home.container'
import User from './pages/User/User.container'
import NewUser from './pages/NewUser/NewUser.container'
import PageNotFound from './pages/PageNotFound/PageNotFound'
import Voting from './pages/Voting/Voting.container'

import withAccessRights from './common/AccessRights/withAccessRights'

import { checkLogin } from './pages/Login/Login.actions'
import Footer from './common/components/Footer/Footer.component'
import Header from './common/components/Header/Header.container'

class App extends Component {
  constructor(props) {
    super(props)

    const { loggedIn, initCheckLogin } = this.props;
    if (!loggedIn) {
      initCheckLogin()
    }
  }
  render() {
    return (
      <React.Fragment>
        <Header />
        <main className='App'>
          <Container>
            <Switch>
              <Route exact path='/' component={Home} />
              <Route path='/user/:id' component={User} />
              <Route path='/add-user' component={this.props.checkAccessRights() ? NewUser : PageNotFound} />
              <Route path='/voting' component={Voting} />
              <Route default component={PageNotFound} />
            </Switch>
          </Container>
        </main>
        <Footer />
      </React.Fragment>
    );
  }
}

const mapStateToProps = ({ login }) => ({
  loggedIn: login.data.loggedIn
})

const mapDispatchesToProps = (dispatch) => ({
  initCheckLogin() {
    dispatch(checkLogin())
  }
})

export default connect(
  mapStateToProps,
  mapDispatchesToProps
) (
  withAccessRights(App)
);
