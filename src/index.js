import React from 'react'
import { render } from 'react-dom'
import './index.css'
import App from './App'
import registerServiceWorker from './registerServiceWorker'
import { Provider } from 'react-redux'
import history from './store/history';
import { ConnectedRouter } from 'connected-react-router'
import configureStore from './store/configureStore'
import { Switch, Route } from 'react-router-dom'
import Login from './pages/Login/Login.container'
import Errors from './common/Dialogs/Errors/Errors.container'

const store = configureStore()

const renderApp = () => render(
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <React.Fragment>
        <Switch>
          <Route path='/admin' component={Login} />
          <Route component={App} />
        </Switch>

        <Errors />
      </React.Fragment>
    </ConnectedRouter>
  </Provider>,
  document.getElementById('root')
)

if (process.env.NODE_ENV !== 'production' && module.hot) {
  module.hot.accept('./App', () => {
    renderApp()
  })
}

renderApp()

registerServiceWorker()