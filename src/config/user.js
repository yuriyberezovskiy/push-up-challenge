export const powerStatuses = [
  'Perfect! Master of push-ups!', // >= 100%
  'Well done! Strongman!', // >= 75% && <100%
  'Middle, good job but can better!', // >= 50% && <75%
  'Junior, need more push-ups!', // >= 25% && <50%
  'Beginner, need more push-ups!' // >= 0% && <25%
]